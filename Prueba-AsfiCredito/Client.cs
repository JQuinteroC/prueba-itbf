using System;

namespace Prueba_AsfiCredito
{
    public class Client
    {
        public Guid Id { get; set; }

        public String Email { get; set; }

        public String Password { get; set; }

        public string UserName { get; set; }
    }
}
