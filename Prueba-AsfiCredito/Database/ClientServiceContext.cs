﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Prueba_AsfiCredito.Database
{
    public class ClientServiceContext : DbContext
    {
        public ClientServiceContext(DbContextOptions<ClientServiceContext> options)
            : base(options)
        {
        }

        public DbSet<Client> Client { get; set; }
    }
}
