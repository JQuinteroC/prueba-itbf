﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba_AsfiCredito.Database
{
    public interface IClientService
    {
        Task InsertClient(Client client);
        Task UpdateClient(Client client);
        Task DeleteClient(Guid id);
        Task<List<Client>> getAllClientes();
        Task<Client> getClientById(Guid id);
        Task<Client> getClienteByEmailAndPassword(string email, string password);
        Task<Client> getClienteByEmail(string email);
    }
}
