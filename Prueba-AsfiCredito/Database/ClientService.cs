﻿using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba_AsfiCredito.Database
{
    public class ClientService : IClientService
    {
        ClientServiceContext context;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ClientService(ClientServiceContext dbcontext)
        {
            context = dbcontext;
        }
        public async Task DeleteClient(Guid id)
        {
            var filter = context.Client.Find(id);
            try
            {
                context.Remove(filter);
                await context.SaveChangesAsync();
                logger.Info("Info: The client has been deleted");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The client could not be deleted, Error: " + e);
            }
        }

        public async Task<Client> getClientById(Guid id)
        {
            return await context.Client.FindAsync(id);
            try
            {
                logger.Info("Info: The client have been obtained successfully");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The client could not be obtained successfully, Error: " + e);
            }
        }

        public async Task<List<Client>> getAllClientes()
        {
            return await context.Client.ToListAsync();
            try
            {
                logger.Info("Info: The clients have been obtained successfully");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The clients could not be obtained successfully, Error: " + e);
            }

        }

        public async Task<Client> getClienteByEmail(string email)
        {
            return await context.Client.FirstOrDefaultAsync(cl => cl.Email == email);
            try
            {
                logger.Info("Info: Client password reset was successful");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: Client password reset was unsuccessful, Error: " + e);
            }
        }

        public async Task<Client> getClienteByEmailAndPassword(string email, string password)
        {
            return await context.Client.FirstOrDefaultAsync<Client>(cl => cl.Email == email && cl.Password == password);
            try
            {
                logger.Info("Info: The client login was successful");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The client login was unsuccessful, Error: " + e);
            }
        }

        public async Task InsertClient(Client client)
        {
            try
            {
                context.Client.Add(client);
                await context.SaveChangesAsync();
                logger.Info("Info: The client was inserted");
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The client was not inserted, Error: " + e);
            }
        }

        public async Task UpdateClient(Client client)
        {
            var filter = context.Client.Find(client.Id);
            try
            {
                if (filter != null)
                {
                    filter.Email = client.Email;
                    filter.Password = client.Password;
                    filter.UserName = client.UserName;

                    await context.SaveChangesAsync();
                    logger.Info("Info: The client was updated");
                }
            }
            catch (Exception e)
            {
                logger.Fatal("Fatal: The client was not updated, Error: " + e);
            }

        }
    }
}
