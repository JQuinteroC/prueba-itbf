﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prueba_AsfiCredito.Database;
using System;
using System.Threading.Tasks;

namespace Prueba_AsfiCredito.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClientController : Controller
    {
        private readonly IClientService clienteServicie;
        public ClientController(IClientService service)
        {
            clienteServicie = service;
        }

        // GET: api/Clients
        [HttpGet]
        public async Task<IActionResult> getAllClientes()
        {
            return Ok(await clienteServicie.getAllClientes());
        }

        [HttpGet("{Email}/{Password}")]
        public async Task<IActionResult> getClienteByEmailAndPassword(string email, string password)
        {
            return Ok(await clienteServicie.getClienteByEmailAndPassword(email, password));
        }

        [HttpGet("{Email}")]
        public async Task<IActionResult> getClienteByEmail(string email)
        {
            return Ok(await clienteServicie.getClienteByEmail(email));
        }

        /// <summary>
        /// Creates a Client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns>A newly created client</returns>
        /// <response code="201">Returns the newly created client</response>
        /// <response code="400">If the client is null</response>  
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> InsertClient([FromBody] Client client)
        {
            if (client == null)
            {
                return BadRequest();
            }
            if (client.UserName == string.Empty)
            {
                ModelState.AddModelError("Nombre", "El nombre del cliente no debe estar vacio");
            }
            if (client.Email == string.Empty)
            {
                ModelState.AddModelError("Email", "El email del cliente no debe estar vacio");
            }
            if (client.Password == string.Empty)
            {
                ModelState.AddModelError("Password", "La contraseña del cliente no debe estar vacia");
            }
            await clienteServicie.InsertClient(client);
            return Created("Cliente Creado", true);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClient([FromBody] Client client, Guid id)
        {
            if (client == null)
            {
                return BadRequest();
            }
            if (client.UserName == string.Empty)
            {
                ModelState.AddModelError("Nombre", "El nombre del cliente no debe estar vacio");
            }
            if (client.Email == string.Empty)
            {
                ModelState.AddModelError("Email", "El email del cliente no debe estar vacio");
            }
            if (client.Password == string.Empty)
            {
                ModelState.AddModelError("Password", "La contraseña del cliente no debe estar vacia");
            }
            client.Id = id;
            await clienteServicie.UpdateClient(client);
            return Created("Cliente Actualizado", true);
        }

        /// <summary>
        /// Deletes a specific Client.
        /// </summary>
        /// <param name="id"></param>    
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient(Guid id)
        {
            await clienteServicie.DeleteClient(id);
            return NoContent();
        }

        [HttpGet("id/{id}")]
        public async Task<IActionResult> clientById(Guid id)
        {
            return Ok(await clienteServicie.getClientById(id));
        }
    }
}
